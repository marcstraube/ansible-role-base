base
=========

Install and configure basic system tools

Example Playbook
----------------

    - hosts: all
      roles:
        - { role: marcstraube.base }

License
-------

GPLv3
